﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[DisallowMultipleComponent]
[RequireComponent(typeof(Rigidbody2D))]
public class SwimAbilityBackup : Ability
{
    #region Public and Serialized Variables

    [Header("Water Detection")]
    [Tooltip("")]
    public LayerMask WaterCheckLayerMask;

    [Range(0, 2), Tooltip("")]
    public float WaterCheckRadius;

    [Range(-2, 2), Tooltip("")]
    public float WaterCheckOffset;


    [Range(0, 20), Tooltip("")]
    public float Speed;

    [Range(0, 20), Tooltip("")]
    public float Strength;

    [Range(0, 100), Tooltip("")]
    public float DashForce;

    #endregion

    #region Properties


    public Vector2 WaterCheckCenter { get { return transform.position + new Vector3(0, WaterCheckOffset); } }
    public RaycastHit2D[] WaterHits { get; private set; }
    public bool IsUnderwater { get; private set; }

    #endregion

    #region Private Variables

    private Rigidbody2D _rb;
    private IEnumerator _lastSwimRoutine;
    private IEnumerator _lastDashRoutine;

    #endregion

    #region Initialization Methods

    private void Start()
    {
        _rb = GetComponent<Rigidbody2D>();
    }



    #endregion

    public void FixedUpdate()
    {
        WaterHits = Physics2D.CircleCastAll(WaterCheckCenter, WaterCheckRadius, Vector2.zero, 0.0f, WaterCheckLayerMask);
        IsUnderwater = WaterHits.Length > 0;
    }

    #region Public Methods

    public void Swim(Vector2 input)
    {
        if (_lastSwimRoutine != null)
            StopCoroutine(_lastSwimRoutine);

        _lastSwimRoutine = SwimRoutine(input);
        StartCoroutine(_lastSwimRoutine);
    }

    public void Dash(Vector2 input)
    {
        if (_lastDashRoutine != null)
            StopCoroutine(_lastDashRoutine);

        _lastDashRoutine = DashRoutine(input);
        StartCoroutine(_lastDashRoutine);
    }

    #endregion

    #region Private Methods

    private Vector2 CalculateSpeed(Vector2 input)
    {
        Vector2 result = Speed * input;

        return result;
    }

    private Vector2 CalculateStrength(Vector2 input)
    {
        Vector2 result = Strength * input;

        return result;
    }

    private IEnumerator SwimRoutine(Vector2 input)
    {
        yield return new WaitForFixedUpdate();

        if (!CanSwim())
            yield break;

        float currentVelocityX = _rb.velocity.x;
        float currentVelocityY = _rb.velocity.y;

        float desiredVelocityX = CalculateSpeed(input).x;
        float desiredVelocityY = CalculateSpeed(input).y;

        float calculatedStrengthX = CalculateStrength(input).x;
        float calculatedStrengthY = CalculateStrength(input).y;

        float neededVelocityX = desiredVelocityX - Mathf.Clamp(currentVelocityX, -Mathf.Abs(desiredVelocityX), Mathf.Abs(desiredVelocityX));
        float neededVelocityY = desiredVelocityY - Mathf.Clamp(currentVelocityY, -Mathf.Abs(desiredVelocityY), Mathf.Abs(desiredVelocityY));

        float cappedVelocityX = Mathf.Clamp(neededVelocityX, -Mathf.Abs(calculatedStrengthX), Mathf.Abs(calculatedStrengthX));
        float cappedVelocityY = Mathf.Clamp(neededVelocityY, -Mathf.Abs(calculatedStrengthY), Mathf.Abs(calculatedStrengthY));

        Vector2 force = new Vector2(cappedVelocityX, cappedVelocityY);

        _rb.AddForce(force, ForceMode2D.Impulse);
    }

    private IEnumerator DashRoutine(Vector2 input)
    {
        yield return new WaitForFixedUpdate();

        if (!CanDash())
            yield break;

        input.y = Mathf.Clamp(input.y, 0.75f, 1.0f);
        input.x = Mathf.Clamp(input.x, -0.75f, 0.75f);

        Vector2 force = input * DashForce;
        _rb.AddForce(force, ForceMode2D.Impulse);

    }


    private bool CanSwim()
    {
        if (IsUnderwater == false)
            return false;
        // TODO if stunned - false
        // TODO if grabbed - false

        return true;
    }

    private bool CanDash()
    {
        if (IsUnderwater == false)
            return false;
        // TODO if stunned - false
        // TODO if grabbed - false

        return true;
    }

    

    #endregion

    #region Debugging

    public void OnDrawGizmos()
    {
        Color color = new Color(0.5f, 0.5f, 0.5f);
        if (IsUnderwater)
            color = new Color(0.0f, 0.0f, 1.0f);
        Color lightColor = color;
        lightColor.a = 0.2f;

        DebugExtension.DrawCircle(WaterCheckCenter, Vector3.forward, color, WaterCheckRadius);
        DebugExtension.DrawPoint(WaterCheckCenter, color, WaterCheckRadius * 2);
        Handles.color = lightColor;
        Handles.DrawSolidDisc(WaterCheckCenter, Vector3.forward, WaterCheckRadius);

        if (WaterHits != null && WaterHits.Length > 0)
        {
            foreach (var hit in WaterHits)
            {
                Debug.DrawLine(WaterCheckCenter, hit.point, color);
                DebugExtension.DrawCircle(hit.point, Vector3.forward, color, 0.1f);
                Debug.DrawLine(hit.point, hit.transform.position, color);
                DebugExtension.DrawCircle(hit.transform.position, Vector3.forward, color, 0.1f);
            }
        }
    }
    
#endregion

}
