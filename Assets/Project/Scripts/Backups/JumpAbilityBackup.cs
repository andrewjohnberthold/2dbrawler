﻿using System;
using UnityEngine;
using System.Collections;
using UnityEditor.VersionControl;


[DisallowMultipleComponent]
[RequireComponent(typeof(PlayerCollisionManager))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class JumpAbilityBackup : MonoBehaviour
{
    #region Public and Serialized Variables

    [Range(0.1f, 5), Tooltip(
        "Minimum height, above the starting height, to accelerate towards. Velocity " +
        "required to reach this height will be applied at the start of a jump, unless limited " +
        "(MaxBoostSupply). Jump apex may be skipped when using slower FixedTimesteps, increase slightly to compensate."
        )]
    public float MinJumpHeight = 1.0f;

    [Range(1, 10), Tooltip(
        "Maximum height, above the starting height, to accelerate towards. Velocity " +
        "required to reach this height will be applied at the end of a jump (MaxJumpDuration), " +
        "unless limited (MaxBoostSupply). Jump apex may be skipped when using slower FixedTimesteps, " +
        "increase slightly to compensate."
        )]
    public float MaxJumpHeight = 3.0f;

    [Range(0.01f, 1.0f),
     Tooltip(
         "Shortest possible jump in seconds. This prevents ground checks from ending the jump " +
         "before the object has had a chance to become airborne."
         )]
    public float MinJumpDuration = 0.1f;

    [Range(0.05f, 5.0f), Tooltip(
        "Limits how long we can boost, in seconds, after starting a jump."
        )]
    public float MaxJumpDuration = 0.5f;

    [Tooltip("This curve curve will automatically scale so that Time:0.0 = 0.0 seconds, Time:1.0 = MaxJumpDuration, " +
             "Value:0 = MinJumpHeight, Value:1 = MaxJumpHeight. Each frame the curve uses CurrentJumpDuration, " +
             "to determine the height to accelerate towards.")]
    public AnimationCurve JumpHeightCurve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);

    [Tooltip("A virtual reserve of energy that is consumed by jumping and boosting. " +
             "The velocity applied by jumping and boosting is limited by the amount left, and then decreases the amount " +
             "left after the force has been applied. The supply is reset when the player is grounded or, if " +
             "ResetBoostOnJump is true, when they start a new jump.")]
    public float MaxBoostSupply = 60.0f;

    [Tooltip("Determines whether or not the first force applied by a jump will be limited by CurrentJumpBoostLeft.")]
    public bool InitialBoostUnlimited = false;

    [Tooltip("Determines whether or not the first force applied by a jump will increase CurrentJumpBoostUsed.")]
    public
        bool InitialBoostFree = false;

    [Tooltip(
        "Determines whether or not CurrentJumpBoostUsed is reset to 0 at the start of each jump. Useful for consistent air jumping (see: MaxJumps)."
        )]
    public bool ResetBoostOnJump = true;

    [Tooltip("How many times the player may jump before needing to return to the ground to reset JumpCount to 0.")]
    public int MaxJumps = 2;

    [Range(0.1f, 5), Tooltip("Minimum time in seconds after jumping before we can jump again.")]
    public float JumpCooldown = 0.2f;

    #endregion

    #region Properties

    public bool IsJumping { get; private set; }
    public int JumpCount { get; private set; }

    public int JumpsRemaining
    {
        get { return Mathf.Clamp(MaxJumps - JumpCount, 0, MaxJumps); }
    }

    public float LastJumpStartTime { get; private set; }
    public Vector2 LastJumpStartPosition { get; private set; }

    public Vector2 LastJumpStartFootPosition
    {
        get { return LastJumpStartPosition + FootOffset; }
    }
    
    public float CurrentJumpHeight
    {
        get { return IsJumping ? CurrentHeight - LastJumpStartPosition.y : 0.0f; }
    }

    public float CurrentJumpHeightProgress
    {
        get { return IsJumping ? (CurrentJumpHeight - MinJumpHeight) / (MaxJumpHeight - MinJumpHeight) : 0.0f; }
    }

    public bool IsBoosting { get; private set; }

    private float _currentJumpBoostUsed;

    public float CurrentJumpBoostUsed
    {
        get { return _currentJumpBoostUsed; }
        private set { _currentJumpBoostUsed = Mathf.Max(0.0f, value); }
    }

    public float CurrentJumpBoostLeft
    {
        get { return Mathf.Max(0.0f, MaxBoostSupply - CurrentJumpBoostUsed); }
    }

    public float CurrentJumpBoostProgress
    {
        get { return (CurrentJumpBoostUsed / MaxBoostSupply); }
    }

    public float CurrentJumpDuration
    {
        get { return IsJumping ? Time.time - LastJumpStartTime : 0.0f; }
    }

    public float CurrentJumpDurationProgress
    {
        get { return IsJumping ? Mathf.Clamp01(CurrentJumpDuration / MaxJumpDuration) : 0.0f; }
    }

    public float FootHeightOffset
    {
        get { return _col.bounds.min.y - CurrentHeight; }
    }

    public Vector2 FootOffset
    {
        get { return new Vector2(0.0f, FootHeightOffset); }
    }

    public Vector2 CurrentFootPosition
    {
        get { return CurrentPosition + FootOffset; }
    }

    public Vector2 CurrentPosition
    {
        get { return transform.position; }
    }

    public float CurrentHeight
    {
        get { return transform.position.y; }
    }

    #endregion

    #region Private Variables

    private PlayerCollisionManager _pcm;
    private Rigidbody2D _rb;
    private Collider2D _col;

    private Vector2 _previousDebugPosition;
    
    #endregion

    #region Initialization Methods

    private void Start()
    {
        _pcm = GetComponent<PlayerCollisionManager>();
        _rb = GetComponent<Rigidbody2D>();
        _col = GetComponent<Collider2D>();
    }

    #endregion

    #region Public Methods

    public void Jump()
    {
        StopCoroutine(JumpRoutine());
        StartCoroutine(JumpRoutine());
    }

    public void Boost()
    {
        StopCoroutine(BoostRoutine());
        StartCoroutine(BoostRoutine());
    }

    #endregion

    #region Jumping Methods

    private void FixedUpdate()
    {
        if ((_pcm.IsColliding(Side.Bottom)) && (CurrentJumpDuration > MinJumpDuration))
            
        {
            IsJumping = false;
            JumpCount = 0;
            CurrentJumpBoostUsed = 0.0f;
        }
    }

    private IEnumerator JumpRoutine()
    {
        yield return new WaitForFixedUpdate();

        if (!CanJump())
            yield break;

        LastJumpStartTime = Time.time;
        LastJumpStartPosition = CurrentPosition;

        IsJumping = true;
        JumpCount++;
        if (ResetBoostOnJump)
            CurrentJumpBoostUsed = 0.0f;
        _previousDebugPosition = transform.position;

        var currentVelocity = _rb.velocity.y;

        var desiredHeight = LastJumpStartPosition.y +
                            ((MaxJumpHeight - MinJumpHeight) * JumpHeightCurve.Evaluate(CurrentJumpDurationProgress)) +
                            MinJumpHeight;
        var heightDifference = Mathf.Max(0.0f, desiredHeight - CurrentHeight);
        var neededVelocity = Mathf.Max(0.0f, Mathf.Sqrt(heightDifference * -2 * Physics2D.gravity.y) - currentVelocity);

        var cappedVelocity = InitialBoostUnlimited ? neededVelocity : Mathf.Min(neededVelocity, CurrentJumpBoostLeft);

        var force = Vector2.up * cappedVelocity;
        
        _rb.AddForce(force, ForceMode2D.Impulse);

        if (!InitialBoostFree)
            CurrentJumpBoostUsed += cappedVelocity;
    }

    private IEnumerator BoostRoutine()
    {
        yield return new WaitForFixedUpdate();

        if (!CanBoost())
            yield break;

        IsBoosting = true;

        var currentVelocity = _rb.velocity.y;

        var desiredHeight = LastJumpStartPosition.y +
                            ((MaxJumpHeight - MinJumpHeight) * JumpHeightCurve.Evaluate(CurrentJumpDurationProgress)) +
                            MinJumpHeight;
        var heightDifference = Mathf.Max(0.0f, desiredHeight - CurrentHeight);
        var neededVelocity = Mathf.Max(0.0f, Mathf.Sqrt(heightDifference * -2 * Physics2D.gravity.y) - currentVelocity);

        var cappedVelocity = Mathf.Min(neededVelocity, CurrentJumpBoostLeft);

        var force = Vector2.up * cappedVelocity;

        CurrentJumpBoostUsed += cappedVelocity;
        
        _rb.AddForce(force, ForceMode2D.Impulse);

        yield return new WaitForFixedUpdate();

        IsBoosting = false;
    }

    private bool CanJump()
    {
        if (MaxJumps <= JumpCount)
            return false;
        if (LastJumpStartTime + JumpCooldown > Time.time)
            return false;
        //if (_pcm.IsUnderwater)
        //    return false;
        return true;
    }

    private bool CanBoost()
    {
        if (!IsJumping)
            return false;
        //if (_pcm.IsUnderwater)
        //   return false;
        if (CurrentJumpDurationProgress >= 1.0f)
            return false;
        return true;
    }

    #endregion

    #region Debuging Methods

    private void OnDrawGizmos()
    {
        if (_col == null)
        {
            _col = GetComponent<Collider2D>();
        }
        DrawDebugJumpTrace();
        DrawDebugJumpScale();
        DrawDebugFootline();
    }

    private void DrawDebugJumpScale()
    {
        int lines = 30;
        float divisionHeight = MaxJumpHeight / lines;

        Vector2 bottom = IsJumping ? LastJumpStartFootPosition : CurrentFootPosition;
        Vector2 min = new Vector2(bottom.x, bottom.y + MinJumpHeight);
        Vector2 max = new Vector2(bottom.x, bottom.y + MaxJumpHeight);


        for (int i = 0; i < Mathf.Min(JumpsRemaining + 1, MaxJumps); i++)
        {
            for (int j = 0; j < lines; j++)
            {
                var redHeight = min.y + (MaxJumpHeight * i);
                var greenHeight = max.y + (MaxJumpHeight * i);
                var drawHeight = bottom.y + (MaxJumpHeight * i) + (divisionHeight * j);
                var from = new Vector2(bottom.x, drawHeight);
                var to = new Vector2(bottom.x, drawHeight + divisionHeight);
                Color color = drawHeight < redHeight
                    ? Color.white
                    : Color.Lerp(Color.green, Color.red, (drawHeight - redHeight) / (greenHeight - redHeight));

                Debug.DrawLine(from, to, color);
            }
            Vector2 jumpcountOffset = new Vector2(0, MaxJumpHeight * i);
            DebugExtension.DrawCircle(bottom + jumpcountOffset, Vector3.forward, Color.white, 0.1f);
            DebugExtension.DrawCircle(min + jumpcountOffset, Vector3.forward, Color.green, 0.1f);
            DebugExtension.DrawCircle(max + jumpcountOffset, Vector3.forward, Color.red, 0.1f);
        }
    }

    private void DrawDebugFootline()
    {
        Vector2 bottom = IsJumping ? LastJumpStartFootPosition : CurrentFootPosition;
        Vector2 min = new Vector2(bottom.x, bottom.y + MinJumpHeight);
        Vector2 max = new Vector2(bottom.x, bottom.y + MaxJumpHeight);

        Vector2 from = CurrentFootPosition;
        Vector2 to = new Vector2(bottom.x, CurrentFootPosition.y);

        Color color;

        if (CurrentFootPosition.y < bottom.y)
        {
            color = Color.black;
            Debug.DrawLine(bottom, to, color);
        }
        else if (CurrentFootPosition.y >= bottom.y && CurrentFootPosition.y < min.y)
            color = Color.white;
        else
            color = Color.Lerp(Color.green, Color.red, (CurrentFootPosition.y - min.y) / (max.y - min.y));

        if (from.x > to.x)
        {
            from.x += 1;
            to.x -= 1;
        }
        else
        {
            from.x -= 1;
            to.x += 1;
        }

        Debug.DrawLine(from, to, color);
        DebugExtension.DrawCircle(new Vector3(bottom.x, CurrentFootPosition.y), Vector3.forward, color, 0.1f);
    }

    private void DrawDebugJumpTrace()
    {
        if (IsJumping)
        {
            Color tracerColor = Color.Lerp(Color.green, Color.red, CurrentJumpBoostProgress);


            if (CurrentJumpBoostProgress >= 1.0f || CurrentJumpDurationProgress >= 1.0f)
                tracerColor = Color.black;
            Vector2 tracerStart = _previousDebugPosition + FootOffset;
            Vector2 tracerEnd = CurrentFootPosition;
            float tracerDuration = MaxJumpDuration * MaxJumps * 3;
            Debug.DrawLine(tracerStart, tracerEnd, tracerColor, tracerDuration);
            _previousDebugPosition = CurrentPosition;
         }
    }

    #endregion 

}
