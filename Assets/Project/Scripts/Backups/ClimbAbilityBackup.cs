﻿using UnityEngine;
using System.Collections;
using System.Linq;
using UnityEditor;

[DisallowMultipleComponent]
[RequireComponent(typeof(PlayerCollisionManager))]
[RequireComponent(typeof(Rigidbody2D))]
public class ClimbAbilityBackup : Ability
{
    #region Public and Serialized Variables

    [Header("Ladder Detection")]
    [Tooltip("")]
    public LayerMask LadderCheckLayerMask;

    [Range(0, 2), Tooltip("")]
    public float LadderCheckRadius;

    [Range(-2, 2), Tooltip("")]
    public float LadderCheckOffset;


    [Range(0, 20), Tooltip("")]
    public float Speed;

    [Range(0, 20), Tooltip("")]
    public float Strength;

    [Range(0, 100), Tooltip("")]
    public float Drag;

    public bool IsClimbing;
    public bool IsDropping;

    #endregion

    #region Properties

    public Vector2 LadderCheckCenter { get { return transform.position + new Vector3(0, LadderCheckOffset); } }
    public RaycastHit2D[] LadderHits { get; private set; }
    public bool IsOnLadder { get; private set; }

    #endregion

    #region Private Variables

    private PlayerCollisionManager _pcm;
    private Rigidbody2D _rb;
    private IEnumerator _lastClimbRoutine;

    private float _initialDrag;
    private float _initialGravityScale;

    #endregion

    #region Initialization Methods

    private void Start()
    {
        _pcm = GetComponent<PlayerCollisionManager>();
        _rb = GetComponent<Rigidbody2D>();

        _initialDrag = _rb.drag;
        _initialGravityScale = _rb.gravityScale;
    }

    #endregion

    #region Public Methods

    public void Climb(Vector2 input)
    {
        if (_lastClimbRoutine != null)
            StopCoroutine(_lastClimbRoutine);

        _lastClimbRoutine = ClimbRoutine(input);
        StartCoroutine(_lastClimbRoutine);
    }

    public void FixedUpdate()
    {
        LadderHits = Physics2D.CircleCastAll(LadderCheckCenter, LadderCheckRadius, Vector2.zero, 0.0f, LadderCheckLayerMask);
        IsOnLadder = LadderHits.Length > 0;

        if (CanClimb())
        {
            _rb.gravityScale = 0.0f;
            _rb.drag = Drag;
        }

        if (!CanClimb())
        {
            _rb.gravityScale = _initialGravityScale;
            _rb.drag = _initialDrag;
            IsClimbing = false;
        }
    }

    public void Drop()
    {
        IsDropping = true;
    }

    public void EndDrop()
    {
        IsDropping = false;
    }

    #endregion

    #region Private Methods

    private Vector2 CalculateSpeed(Vector2 input)
    {
        Vector2 result = Speed * input;

        return result;
    }

    private Vector2 CalculateStrength(Vector2 input)
    {
        Vector2 result = Strength * input;

        return result;
    }

    private IEnumerator ClimbRoutine(Vector2 input)
    {
        yield return new WaitForFixedUpdate();

        if (!CanClimb())
            yield break;
        if (!CanClimbVert())
            input.y = 0.0f;
        if (!CanClimbAcross())
            input.x = 0.0f;
        
        IsClimbing = true;

        float currentVelocityX = _rb.velocity.x;
        float currentVelocityY = _rb.velocity.y;

        float desiredVelocityX = CalculateSpeed(input).x;
        float desiredVelocityY = CalculateSpeed(input).y;

        float calculatedStrengthX = CalculateStrength(input).x;
        float calculatedStrengthY = CalculateStrength(input).y;

        float neededVelocityX = desiredVelocityX - Mathf.Clamp(currentVelocityX, -Mathf.Abs(desiredVelocityX), Mathf.Abs(desiredVelocityX));
        float neededVelocityY = desiredVelocityY - Mathf.Clamp(currentVelocityY, -Mathf.Abs(desiredVelocityY), Mathf.Abs(desiredVelocityY));

        float cappedVelocityX = Mathf.Clamp(neededVelocityX, -Mathf.Abs(calculatedStrengthX), Mathf.Abs(calculatedStrengthX));
        float cappedVelocityY = Mathf.Clamp(neededVelocityY, -Mathf.Abs(calculatedStrengthY), Mathf.Abs(calculatedStrengthY));

        Vector2 force = new Vector2(cappedVelocityX, cappedVelocityY);

        _rb.AddForce(force, ForceMode2D.Impulse);

    }
    
    private bool CanClimb()
    {
        if (!IsOnLadder)
            return false;
        if (IsDropping)
            return false;

        //not if stunned
        //not if grabbed

        //return true;
        //TODO
        return true;
    }

    private bool CanClimbVert()
    {
        //TODO
        return true;
    }
    private bool CanClimbAcross()
    {
        if (_pcm.IsColliding(Side.Bottom))
           return false;
        return true;
    }

    #endregion

    #region Debugging Methods

    public void OnDrawGizmos()
    {
        Color color = new Color(0.5f, 0.5f, 0.5f);
        if (IsOnLadder)
            color = new Color(0.0f, 1f, 0.0f);
        Color lightColor = color;
        lightColor.a = 0.2f;
        
        DebugExtension.DrawCircle(LadderCheckCenter,Vector3.forward,color,LadderCheckRadius);
        DebugExtension.DrawPoint(LadderCheckCenter, color,LadderCheckRadius*2);
        Handles.color = lightColor;
        Handles.DrawSolidDisc(LadderCheckCenter, Vector3.forward, LadderCheckRadius);

        if (LadderHits != null && LadderHits.Length > 0)
        {
            foreach (var hit in LadderHits)
            {
                Debug.DrawLine(LadderCheckCenter, hit.point, color);
                DebugExtension.DrawCircle(hit.point, Vector3.forward, color, 0.1f);
                Debug.DrawLine(hit.point, hit.transform.position, color);
                DebugExtension.DrawCircle(hit.transform.position, Vector3.forward, color, 0.1f);
            }
        }
    }

    #endregion

    
}
