﻿using System.Collections.Generic;
using System.Linq;
using ExtensionMethods;
using UnityEditor;
using UnityEngine;

public enum Side { None, Top, Right, Bottom, Left}


public class PlayerCollision
{
    public RaycastHit2D Hit { get; private set; }
    public PlayerCollisionManager Creator { get; private set; }
    public float DetectionTimeStamp { get; private set; }
    public Side Side { get; private set; }
    public bool HitInsideRing { get; private set; }
    public bool HitPlatform { get; private set; }
    public bool HitPlatformSurface { get; private set; }
    public bool HitExcludedLayer { get; private set; }
    public bool HitExcludedTag { get; private set; }
    public bool HitHeldObject { get; private set; }
    public bool Valid { get; private set; }

    public bool Invalid
    {
        get { return !Valid; }
    }

    public PlayerCollision(RaycastHit2D hit, PlayerCollisionManager creator)
    {
        Hit = hit;
        Creator = creator;
        DetectionTimeStamp = Time.time;

        Side = GetHitSide();
        HitInsideRing = IsHittingInsideRing();
        PlatformEffector2D platform = null;
        HitPlatform = IsHittingPlatform(out platform);
        if (platform == null)
            HitPlatformSurface = false;
        else
            HitPlatformSurface = IsHittingPlatformSurface(platform);
        HitExcludedLayer = IsHittingExcludedLayer();
        HitExcludedTag = IsHittingExcludedTag();
        HitHeldObject = IsHittingHeldObject();

        Valid = true;
        if (Side == Side.None)
            Valid = false;
        if (HitInsideRing)
            Valid = false;
        if (HitPlatform && !HitPlatformSurface)
            Valid = false;
        if (HitExcludedLayer)
            Valid = false;
        if (HitExcludedTag)
            Valid = false;
        if (HitHeldObject)
            Valid = false;

        //Debug.Log("HitPlatform: " + HitPlatform + ". HitPlatformSurface: " + HitPlatformSurface+ ". Valid: "+Valid);
    }

    private Side GetHitSide()
    {
        Vector3 directionToPoint = (Hit.point - (Vector2) Creator.transform.position).normalized;
        float topLeft = Vector3.Angle(Creator.TopLeftDirection, directionToPoint);
        float topRight = Vector3.Angle(Creator.TopRightDirection, directionToPoint);
        float bottomRight = Vector3.Angle(Creator.BottomRightDirection, directionToPoint);
        float bottomLeft = Vector3.Angle(Creator.BottomLeftDirection, directionToPoint);

        if (topLeft <= Creator.TopSliceDegrees && topRight <= Creator.TopSliceDegrees)
            return Side.Top;
        if (bottomRight <= Creator.BottomSliceDegrees && bottomLeft <= Creator.BottomSliceDegrees)
            return Side.Bottom;
        if (topLeft < Creator.SideSliceDegrees && bottomLeft < Creator.SideSliceDegrees)
            return Side.Left;
        if (topRight < Creator.SideSliceDegrees && bottomRight < Creator.SideSliceDegrees)
            return Side.Right;
        return Side.None;
    }

    private bool IsHittingInsideRing()
    {
        return Vector2.Distance(Hit.point, Creator.transform.position) < Creator.InnerRadius;
    }

    private bool IsHittingPlatform(out PlatformEffector2D platform)
    {
        platform = null;

        if (Hit.collider.usedByEffector == false)
            return false;

        platform = Hit.transform.gameObject.GetComponent<PlatformEffector2D>();
        if (platform == null)
            return false;
        if (!platform.isActiveAndEnabled)
            return false;
        
        
        return true;
    }

    private bool IsHittingPlatformSurface(PlatformEffector2D platform)
    {
        if (!HitPlatform)
            return false;

        if (platform.transform != Hit.transform)
            return false;
        
        if (platform.useColliderMask)
        {
            LayerMask platformMask = platform.colliderMask;
            int playerLayer = Creator.gameObject.layer;

            if (!platformMask.IncludesLayer(playerLayer))
            {
                return false;
            }
        }

        if (platform.useOneWay)
        {
            Vector2 platformHitNormal = Hit.transform.InverseTransformDirection(Hit.normal);
            float platformHitAngle = Mathf.Atan2(platformHitNormal.x, platformHitNormal.y) * 180 / Mathf.PI;
            float arcAngle = platform.surfaceArc;
            float arcLeftAngle = -(arcAngle / 2);
            float arcRightAngle = arcAngle / 2;

            if (platformHitAngle <= arcLeftAngle || platformHitAngle >= arcRightAngle)
            {
                return false;
            }
        }

        
        
        return true;
    }

    private bool IsHittingExcludedLayer()
    {
        if (!Creator.IncludedLayers.IncludesLayer(Hit.transform.gameObject))
            return true;

        switch (Side)
        {
            case Side.None:
                return false;
            case Side.Top:
                return Creator.ExcludedLayersTop.IncludesLayer(Hit.transform.gameObject);
            case Side.Bottom:
                return Creator.ExcludedLayersBottom.IncludesLayer(Hit.transform.gameObject);
            case Side.Right:
            case Side.Left:
                return Creator.ExcludedLayersSide.IncludesLayer(Hit.transform.gameObject);
            default:
                return false;
        }
    }

    private bool IsHittingExcludedTag()
    {
        switch (Side)
        {
            case Side.None:
                return false;
            case Side.Top:
                return Creator.ExcludedTagsTop.Contains(Hit.transform.tag);
            case Side.Bottom:
                return Creator.ExcludedTagsBottom.Contains(Hit.transform.tag);
            case Side.Right:
            case Side.Left:
                return Creator.ExcludedTagsSide.Contains(Hit.transform.tag);
            default:
                return false;
        }
    }

    private bool IsHittingHeldObject()
    {
        return Creator.HeldCollider2D == Hit.collider;
    }
}

[RequireComponent(typeof(CircleCollider2D))]
public class PlayerCollisionManager : MonoBehaviour
{
    #region Public and Serialized Variables
    [Header("Collision Ring")]
    [Range(0, 2), Tooltip("")]
    public float OuterMargin = 0.05f;

    [Range(0, 2), Tooltip("")]
    public float InnerMargin = 0.05f;

    [Header("Ring Divisions")]
    [Range(0, 180), Tooltip("")]
    public float TopSliceDegrees = 90;

    [Range(0, 180), Tooltip("")]
    public float BottomSliceDegrees = 90;

    [Header("Layer Stuff")]
    [Tooltip("")]
    public LayerMask IncludedLayers;

    [Header("Top")]
    [Tooltip("")]
    public LayerMask ExcludedLayersTop;
    [Tooltip("")]
    public string[] ExcludedTagsTop;

    [Header("Side")]
    [Tooltip("")]
    public LayerMask ExcludedLayersSide;
    [Tooltip("")]
    public string[] ExcludedTagsSide;

    [Header("Bottom")]
    [Tooltip("")]
    public LayerMask ExcludedLayersBottom;
    [Tooltip("")]
    public string[] ExcludedTagsBottom;
    
    #endregion

    public float SideSliceDegrees
    {
        get { return (360 - (TopSliceDegrees + BottomSliceDegrees)) / 2.0f; }
    }
    public float OuterRadius
    {
        get { return Collider.radius + OuterMargin; }
    }
    public float InnerRadius
    {
        get { return Collider.radius - InnerMargin; }
    }

    public float TopLeftAngle
    {
        // TODO this number is negative, the rest are all positive... try to fix
        get { return -TopSliceDegrees / 2; }
    }
    public float TopRightAngle
    {
        get { return TopSliceDegrees / 2; }
    }
    public float BottomRightAngle
    {
        get { return 180 - (BottomSliceDegrees / 2); }
    }
    public float BottomLeftAngle
    {
        get { return 180 + (BottomSliceDegrees / 2); }
    }

    public Vector3 TopLeftDirection
    {
        get{ return new Vector2(Mathf.Sin(TopLeftAngle * Mathf.Deg2Rad), Mathf.Cos(TopLeftAngle * Mathf.Deg2Rad));}
    }
    public Vector3 TopRightDirection
    {
        get { return new Vector2(Mathf.Sin(TopRightAngle * Mathf.Deg2Rad), Mathf.Cos(TopRightAngle * Mathf.Deg2Rad)); }
    }
    public Vector3 BottomRightDirection
    {
        get { return new Vector2(Mathf.Sin(BottomRightAngle * Mathf.Deg2Rad), Mathf.Cos(BottomRightAngle * Mathf.Deg2Rad)); }
    }
    public Vector3 BottomLeftDirection
    {
        get { return new Vector2(Mathf.Sin(BottomLeftAngle * Mathf.Deg2Rad), Mathf.Cos(BottomLeftAngle * Mathf.Deg2Rad)); }
    }
    
    public CircleCollider2D Collider
    {
        get
        {
            return _collider ? _collider : GetComponent<CircleCollider2D>();
        }
    }
    public Collider2D HeldCollider2D
    {
        get { return _grabAbility.HeldCollider2D; }
    }

    #region Private Properties & Variables

    private List<PlayerCollision> _collisions = new List<PlayerCollision>();
    private CircleCollider2D _collider;
    private GrabAbility _grabAbility;

    #endregion

    #region Initialization Methods

    public void Start()
    {
        _collider = GetComponent<CircleCollider2D>();
        _grabAbility = GetComponent<GrabAbility>();
    }

    #endregion

    #region Update Methods

    public void FixedUpdate()
    {
        UpdateCollisions();
    }

    #endregion

    #region Public Methods

    public List<PlayerCollision> GetCollisions(Side[] sides, bool validColliders = true,
        bool invalidColliders = true, bool updateCollisions = false)
    {
        if (updateCollisions)
            UpdateCollisions();

        var results =
            _collisions.Where(
                c => ((c.Valid == validColliders) || (c.Invalid == invalidColliders)) && (sides.Contains(c.Side)));
        return results.ToList();
    }

    public List<PlayerCollision> GetCollisions(Side side, bool validColliders = true, bool invalidColliders = true,
        bool updateCollisions = false)
    {
        Side[] sides = new[] {side};
        return GetCollisions(sides, validColliders, invalidColliders, updateCollisions);
    }

    public List<PlayerCollision> GetCollisions(bool validColliders = true, bool invalidColliders = true,
        bool updateCollisions = false)
    {
        Side[] sides = { Side.Top, Side.Right, Side.Bottom, Side.Left };
        return GetCollisions(sides, validColliders, invalidColliders, updateCollisions);
    }

    public bool IsColliding(Side[] sides, bool updateCollisions = false)
    {
        return GetCollisions(sides, true, false, updateCollisions).Count > 0;
    }

    public bool IsColliding(Side side, bool updateCollisions = false)
    {
        Side[] sides = { side };
        return IsColliding(sides, updateCollisions);
    }

    public bool IsColliding(bool updateCollisions = false)
    {
        Side[] sides = { Side.Top, Side.Right, Side.Bottom, Side.Left };
        return IsColliding(sides, updateCollisions);
    }

    #endregion


    #region Private Methods

    private void UpdateCollisions()
    {
        _collisions.Clear();

        List<RaycastHit2D> hits = Physics2D.CircleCastAll(transform.position, OuterRadius, Vector2.zero, 0.0f, IncludedLayers).ToList();

        foreach (var hit in hits.Where(hit =>
            hit.collider != Collider &&
            !Physics2D.GetIgnoreCollision(hit.collider, Collider) &&
            !hit.collider.isTrigger
            ))
        {
            var collision = new PlayerCollision(hit, this);
            _collisions.Add(collision);
        }

    }

    #endregion


    #region Debuging & Editor Methods

    public void OnDrawGizmos()
    {
        Color validColor = Color.green;
        Color invalidColor = Color.yellow;
        Color neutralColor = Color.white;
        Color activeColor;

        //draw top
        List<PlayerCollision> allTopCollisions = GetCollisions(Side.Top);
        List<PlayerCollision> validTopCollisions = GetCollisions(Side.Top, true, false);
        activeColor = allTopCollisions.Count > 0 ? invalidColor : neutralColor;
        if(validTopCollisions.Count > 0) { 
                activeColor = validColor;
        }
        DrawSlice(TopLeftDirection, TopRightDirection, TopSliceDegrees, activeColor);

        //draw right
        List<PlayerCollision> allRightCollisions = GetCollisions(Side.Right);
        List<PlayerCollision> validRightCollisions = GetCollisions(Side.Right, true, false);
        activeColor = allRightCollisions.Count > 0 ? invalidColor : neutralColor;
        if (validRightCollisions.Count > 0)
        {
            activeColor = validColor;
        }
        DrawSlice(TopRightDirection, BottomRightDirection, SideSliceDegrees, activeColor);

        //draw bottom
        List<PlayerCollision> allBottomCollisions = GetCollisions(Side.Bottom);
        List<PlayerCollision> validBottomCollisions = GetCollisions(Side.Bottom, true, false);
        activeColor = allBottomCollisions.Count > 0 ? invalidColor : neutralColor;
        if (validBottomCollisions.Count > 0)
        {
            activeColor = validColor;
        }
        DrawSlice(BottomRightDirection, BottomLeftDirection, BottomSliceDegrees, activeColor);

        //draw left
        List<PlayerCollision> allLeftCollisions = GetCollisions(Side.Left);
        List<PlayerCollision> validLeftCollisions = GetCollisions(Side.Left, true, false);
        activeColor = allLeftCollisions.Count > 0 ? invalidColor : neutralColor;
        if (validLeftCollisions.Count > 0)
        {
            activeColor = validColor;
        }
        DrawSlice(BottomLeftDirection, TopLeftDirection, SideSliceDegrees, activeColor);

        List<PlayerCollision> allCollisions = GetCollisions();
        foreach (PlayerCollision col in allCollisions)
        {
            activeColor = col.Valid ? validColor : invalidColor;
            DebugExtension.DebugCircle(col.Hit.point, Vector3.forward, activeColor, 0.1f);
            DebugExtension.DebugPoint(col.Hit.point, activeColor, 0.2f);

            Debug.DrawLine(col.Hit.point, col.Hit.transform.position, activeColor);
            DebugExtension.DebugCircle(col.Hit.transform.position, Vector3.forward, activeColor, 0.1f);
            DebugExtension.DebugPoint(col.Hit.transform.position, activeColor, 0.2f);
        }
    }

    private void DrawSlice(Vector3 startDirection, Vector3 endDirection, float angle, Color color)
    {
        Vector3 normal = Vector3.back;

        Handles.color = new Color(color.r, color.g, color.b, 0.2f);
        Handles.DrawSolidArc(transform.position, normal, startDirection, angle, OuterRadius);

        Handles.color = new Color(0.0f, 0.0f, 0.0f, 0.2f);
        Handles.DrawSolidArc(transform.position, normal, startDirection, angle, InnerRadius);

        Handles.color = color;
        Handles.DrawWireArc(transform.position, normal, startDirection, angle, OuterRadius);
        Handles.DrawWireArc(transform.position, normal, startDirection, angle, InnerRadius);
        Handles.DrawDottedLine(transform.position, transform.position + startDirection * OuterRadius, 3.0f);
        Handles.DrawDottedLine(transform.position, transform.position + endDirection * OuterRadius, 3.0f);
    }

    #endregion

}
