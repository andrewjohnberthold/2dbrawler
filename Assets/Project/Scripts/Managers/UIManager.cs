﻿using UnityEngine;
using System.Collections;

public class UIManager : Singleton<UIManager>
{
    // guarantee this will be always a singleton only - can't use the constructor!
    protected UIManager() { }

}
