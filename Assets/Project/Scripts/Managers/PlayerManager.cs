﻿using UnityEngine;
using System.Collections;

public class PlayerManager : Singleton<PlayerManager>
{
    // guarantee this will be always a singleton only - can't use the constructor!
    protected PlayerManager() { }
}
