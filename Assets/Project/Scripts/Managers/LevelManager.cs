﻿using UnityEngine;
using System.Collections;

public class LevelManager : Singleton<LevelManager>
{
    // guarantee this will be always a singleton only - can't use the constructor!
    protected LevelManager() { }
}
