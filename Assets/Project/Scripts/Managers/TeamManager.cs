﻿using UnityEngine;
using System.Collections;

public class TeamManager : Singleton<TeamManager>
{
    // guarantee this will be always a singleton only - can't use the constructor!
    protected TeamManager() { }

}
