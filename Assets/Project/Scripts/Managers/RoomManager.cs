﻿using UnityEngine;
using System.Collections;

public class RoomManager : Singleton<RoomManager>
{
    // guarantee this will be always a singleton only - can't use the constructor!
    protected RoomManager() { }

}
