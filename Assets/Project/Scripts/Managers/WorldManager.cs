﻿using UnityEngine;
using System.Collections;

public class WorldManager : Singleton<WorldManager>
{
    // guarantee this will be always a singleton only - can't use the constructor!
    protected WorldManager() { }

}
