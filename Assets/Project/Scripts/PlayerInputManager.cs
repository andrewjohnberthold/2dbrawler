﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof (WalkAbility))]
[RequireComponent(typeof (AirWalkAbility))]
[RequireComponent(typeof (AttackAbility))]
[RequireComponent(typeof (JumpAbility))]
[RequireComponent(typeof (GrabAbility))]
[RequireComponent(typeof (DropAbility))]
//[RequireComponent(typeof(Grabbable))]

public class PlayerInputManager : MonoBehaviour
{
    [Range(1, 8)] public int PlayerNumber = 1;

    private WalkAbility _walkAbility;
    private AirWalkAbility _airWalkAbility;
    private AttackAbility _attackAbility;
    private JumpAbility _jumpAbility;
    private GrabAbility _grabAbility;
    private DropAbility _dropAbility;
    //private Grabbable _grabbable;

    private void Start()
    {
        _walkAbility = GetComponent<WalkAbility>();
        _airWalkAbility = GetComponent<AirWalkAbility>();
        _attackAbility = GetComponent<AttackAbility>();
        _jumpAbility = GetComponent<JumpAbility>();
        _grabAbility = GetComponent<GrabAbility>();
        _dropAbility = GetComponent<DropAbility>();
        //_grabbable = GetComponent<Grabbable>();
    }

    private void Update()
    {
        float x1 = Input.GetAxis("P" + PlayerNumber + " Horizontal");
        float x2 = Input.GetAxis("P" + PlayerNumber + " Horizontal Alt");
        float y1 = Input.GetAxis("P" + PlayerNumber + " Vertical");
        float y2 = Input.GetAxis("P" + PlayerNumber + " Vertical Alt");

        float xInput = (Mathf.Abs(x1) > Mathf.Abs(x2)) ? x1 : x2;
        float yInput = (Mathf.Abs(y1) > Mathf.Abs(y2)) ? y1 : y2;

        Vector2 axisInput = new Vector2(xInput,yInput);
        Vector2 input = Vector2.ClampMagnitude(axisInput, 1.0f);

        _walkAbility.Walk(input.x);
        _airWalkAbility.AirWalk(input);
        _grabAbility.Aim(input);
        _attackAbility.Aim(input);

        if (Input.GetButtonDown("P" + PlayerNumber + " Attack/Throw"))
        {

            _grabAbility.Throw();
            _attackAbility.Attack();
        }

        if (Input.GetButtonDown("P" + PlayerNumber + " Jump/Escape"))
        {
            _jumpAbility.Jump();

            //_grabbable.Escape();
        }
        else if (Input.GetButton("P" + PlayerNumber + " Jump/Escape"))
        {
            _jumpAbility.Boost();
            
        }
        if (Input.GetButtonDown("P" + PlayerNumber + " Grab"))
        {
            _dropAbility.Drop(input.y);
            _grabAbility.Grab();

        }

    }
}

