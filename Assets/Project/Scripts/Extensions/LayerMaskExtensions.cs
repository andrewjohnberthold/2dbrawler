﻿using UnityEngine;
using System.Collections;

namespace ExtensionMethods
{

    public static class LayerMaskExtensions
    {
        public static bool IncludesLayer(this LayerMask mask, int layer)
        {
            return ((mask.value & (1 << layer)) > 0);
        }

        public static bool IncludesLayer(this LayerMask mask, GameObject obj)
        {
            return ((mask.value & (1 << obj.layer)) > 0);
        }
    }
}
