﻿using UnityEngine;
using System.Collections;

public class Grabbable : MonoBehaviour
{

    public bool IsThrown;

    public bool IsGrabbed;

    public bool IsGrabbable
    {
        get
        {
            //if blah(){
            //return false;
            //}
            return true;
        }
    }

    public bool IsThrowable
    {
        get
        {
            //if blah(){
            //return false;
            //}
            return true;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (IsThrown)
            IsThrown = false;
    }
}
