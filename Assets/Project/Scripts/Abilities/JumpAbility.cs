﻿using System;
using UnityEngine;
using System.Collections;
using UnityEditor.VersionControl;
using UnityEngine.EventSystems;


[DisallowMultipleComponent]
[RequireComponent(typeof(PlayerCollisionManager))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
public class JumpAbility : MonoBehaviour
{
    #region Public and Serialized Variables
    [Tooltip("")]
    public float JumpForce = 3.0f;

    [Range(0.01f, 1.0f),
     Tooltip(
         "Shortest possible jump in seconds. This prevents ground checks from ending the jump " +
         "before the object has had a chance to become airborne."
         )]
    public float MinJumpDuration = 0.1f;

    [Range(0.05f, 5.0f), Tooltip("Limits how long we can boost, in seconds, after starting a jump.")]
    public float MaxJumpDuration = 0.5f;
    
    [Tooltip("")]
    public float MinBoostForce = 3.0f;

    [Tooltip("")]
    public float MaxBoostForce = 3.0f;

    [Tooltip("")]
    public AnimationCurve BoostForceCurve = AnimationCurve.Linear(0.0f, 0.0f, 1.0f, 1.0f);
    
    [Tooltip("")]
    public bool ResetVelocityOnJump = false;

    [Range(0,1), Tooltip("")]
    public float MassScalar;

    [Tooltip("How many times the player may jump before needing to return to the ground to reset JumpCount to 0.")]
    public int MaxJumps = 2;

    [Range(0.1f, 5), Tooltip("Minimum time in seconds after jumping before we can jump again.")]
    public float JumpCooldown = 0.2f;

    #endregion

    #region Properties

    public bool IsJumping { get; private set; }
    public int JumpCount { get; private set; }

    public int JumpsRemaining
    {
        get { return Mathf.Clamp(MaxJumps - JumpCount, 0, MaxJumps); }
    }

    public float LastJumpStartTime { get; private set; }
    public Vector2 LastJumpStartPosition { get; private set; }

    public Vector2 LastJumpStartFootPosition
    {
        get { return LastJumpStartPosition + FootOffset; }
    }

    public bool IsBoosting { get; private set; }

    public float CurrentJumpDuration
    {
        get { return IsJumping ? Time.time - LastJumpStartTime : 0.0f; }
    }

    public float CurrentJumpDurationProgress
    {
        get { return IsJumping ? Mathf.Clamp01(CurrentJumpDuration / MaxJumpDuration) : 0.0f; }
    }

    public float FootHeightOffset
    {
        get { return _col.bounds.min.y - CurrentHeight; }
    }

    public Vector2 FootOffset
    {
        get { return new Vector2(0.0f, FootHeightOffset); }
    }

    public Vector2 CurrentFootPosition
    {
        get { return CurrentPosition + FootOffset; }
    }

    public Vector2 CurrentPosition
    {
        get { return transform.position; }
    }

    public float CurrentHeight
    {
        get { return transform.position.y; }
    }

    #endregion

    #region Private Variables

    private PlayerCollisionManager _pcm;
    private GrabAbility _grabAbility;
    private Rigidbody2D _rb;
    private Collider2D _col;

    private Vector2 _previousDebugPosition;
    
    #endregion

    #region Initialization Methods

    private void Start()
    {
        _pcm = GetComponent<PlayerCollisionManager>();
        _grabAbility = GetComponent<GrabAbility>();
        _rb = GetComponent<Rigidbody2D>();
        _col = GetComponent<Collider2D>();
    }

    #endregion

    #region Public Methods

    public void Jump()
    {
        StopCoroutine(JumpRoutine());
        StartCoroutine(JumpRoutine());
    }

    public void Boost()
    {
        StopCoroutine(BoostRoutine());
        StartCoroutine(BoostRoutine());
    }

    #endregion

    #region Jumping Methods

    private void FixedUpdate()
    {
        if (_pcm.IsColliding(Side.Bottom) && (CurrentJumpDuration > MinJumpDuration))
            
        {
            IsJumping = false;
            JumpCount = 0;
        }
    }

    private IEnumerator JumpRoutine()
    {
        yield return new WaitForFixedUpdate();

        if (!CanJump())
            yield break;

        LastJumpStartTime = Time.time;
        LastJumpStartPosition = CurrentPosition;

        IsJumping = true;
        JumpCount++;
        _previousDebugPosition = transform.position;

        
        if (ResetVelocityOnJump)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, 0.0f);
        }

        var totalMass = _rb.mass;
        if (_grabAbility.IsHolding)
            totalMass += _grabAbility.HeldRigidbody2D.mass;

        var forceAffectedByMass = Vector2.up * JumpForce;
        var forceUnaffectedByMass = forceAffectedByMass * totalMass;
        var scaledForce = Vector2.Lerp(forceUnaffectedByMass, forceAffectedByMass, MassScalar);

        _rb.AddForce(scaledForce, ForceMode2D.Force);
    }

    private IEnumerator BoostRoutine()
    {
        yield return new WaitForFixedUpdate();

        if (!CanBoost())
            yield break;

        IsBoosting = true;

        var curveEvaluatedBoost = ((MaxBoostForce - MinBoostForce) * BoostForceCurve.Evaluate(CurrentJumpDurationProgress)) + MinBoostForce;

        var totalMass = _rb.mass;
        if (_grabAbility.IsHolding)
            totalMass += _grabAbility.HeldRigidbody2D.mass;

        var forceAffectedByMass = Vector2.up * curveEvaluatedBoost;
        var forceUnaffectedByMass = forceAffectedByMass * totalMass;
        var scaledForce = Vector2.Lerp(forceUnaffectedByMass, forceAffectedByMass, MassScalar);

        _rb.AddForce(scaledForce, ForceMode2D.Force);

        yield return new WaitForFixedUpdate();

        IsBoosting = false;
    }

    private bool CanJump()
    {
        if (MaxJumps <= JumpCount)
            return false;
        if (LastJumpStartTime + JumpCooldown > Time.time)
            return false;
        //if (_pcm.IsUnderwater)
        //    return false;
        return true;
    }

    private bool CanBoost()
    {
        if (!IsJumping)
            return false;
        //if (_pcm.IsUnderwater)
        //   return false;
        if (CurrentJumpDurationProgress >= 1.0f)
            return false;
        return true;
    }

    #endregion

    #region Debuging Methods

    private void OnDrawGizmos()
    {
        if (_col == null)
        {
            _col = GetComponent<Collider2D>();
        }
        DrawDebugJumpTrace();
    }

    private void DrawDebugJumpTrace()
    {
        if (IsJumping)
        {
            Color tracerColor = Color.Lerp(Color.green, Color.red, CurrentJumpDurationProgress);


            if (CurrentJumpDurationProgress >= 1.0f)
                tracerColor = Color.black;
            Vector2 tracerStart = _previousDebugPosition + FootOffset;
            Vector2 tracerEnd = CurrentFootPosition;
            float tracerDuration = MaxJumpDuration * MaxJumps * 3;
            Debug.DrawLine(tracerStart, tracerEnd, tracerColor, tracerDuration);
            _previousDebugPosition = CurrentPosition;
         }
    }

    #endregion 

}
