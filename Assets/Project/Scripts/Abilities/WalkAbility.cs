﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
[RequireComponent(typeof(PlayerCollisionManager))]
[RequireComponent(typeof(PlayerStateManager))]
[RequireComponent(typeof(Rigidbody2D))]
public class WalkAbility : Ability
{
    
    //TODO
    //connect to animation controller

    //TODO
    //footstep sounds

    #region Public and Serialized Variables

    [Tooltip("")]
    public float BaseSpeed;

    public float MinStrength;

    [Tooltip("")]
    public float MaxStrength;

    
    
    #endregion

    #region Properties
    public float AverageSurfaceSpeed { get; private set; }
    public float AverageSurfaceFriction { get; private set; }
    
    #endregion

    #region Private Variables

    private PlayerCollisionManager _pcm;
    private PlayerStateManager _psm;
    private Rigidbody2D _rb;
    private IEnumerator _lastWalkRoutine;

    #endregion

    #region Initialization Methods

    private void Start()
    {
        _pcm = GetComponent<PlayerCollisionManager>();
        _psm = GetComponent<PlayerStateManager>();
        _rb = GetComponent<Rigidbody2D>();
    }

    #endregion

    public void FixedUpdate()
    {
        UpdateSurfaceInfo();
    }

    #region Public Methods

    public void Walk(float input)
    {
        if (_lastWalkRoutine != null)
            StopCoroutine(_lastWalkRoutine);

        _lastWalkRoutine = WalkRoutine(input);
        StartCoroutine(_lastWalkRoutine);
    }

    #endregion

    #region Private Methods

    private float CalculateSpeed(float input)
    {
        float result = BaseSpeed;
        result *= input;

        if (result > 0.0f && AverageSurfaceSpeed > 0.0f)
            result += AverageSurfaceSpeed;
        if (result < 0.0f && AverageSurfaceSpeed < 0.0f)
            result += AverageSurfaceSpeed;

        return result;
    }

    private float CalculateStrength(float input)
    {
        float result = MaxStrength;
        result *= input;
        result *= Mathf.Clamp(AverageSurfaceFriction, 0.0f, 1.0f);

        if(input > 0)
            result = Mathf.Max(result, MinStrength);
        if (input < 0)
            result = Mathf.Min(result, -MinStrength);

        return result;
    }

    private IEnumerator WalkRoutine(float input)
    {
        yield return new WaitForFixedUpdate();

        if (!CanWalk())
            yield break;

        if (input < 0)
            _psm.FacingDirection = Facing.Left;
        else if (input > 0)
            _psm.FacingDirection = Facing.Right;

        float currentVelocity = _rb.velocity.x;

        float desiredVelocity = CalculateSpeed(input);

        float neededVelocity = desiredVelocity - Mathf.Clamp(currentVelocity, -Mathf.Abs(desiredVelocity), Mathf.Abs(desiredVelocity));

        float cappedVelocity = Mathf.Clamp(neededVelocity, -Mathf.Abs(CalculateStrength(input)), Mathf.Abs(CalculateStrength(input)));

        _rb.AddForce(Vector2.right * cappedVelocity, ForceMode2D.Impulse);
    }


    private bool CanWalk()
    {
        if (_pcm.IsColliding(Side.Bottom) == false)
            return false;
        return true;
    }

    private void UpdateSurfaceInfo()
    {
        float totalFriction = 0.0f;
        float totalSpeed = 0.0f;
        var cols = _pcm.GetCollisions(Side.Bottom, true, false);
        foreach (var col in cols)
        {
            bool useFriction = true;
            if (col.Hit.collider.usedByEffector)
            {
                var surfaceEffector2D = col.Hit.transform.GetComponent<SurfaceEffector2D>();
                if (surfaceEffector2D != null)
                {
                    totalSpeed += surfaceEffector2D.speed;
                    useFriction = surfaceEffector2D.useFriction;
                }
            }

            //var rb = col.Hit.rigidbody;
            //if (rb != null)
            //    totalSpeed += rb.velocity.x;

            var material = col.Hit.collider.sharedMaterial;
            if (material != null && useFriction)
                totalFriction += material.friction;
        }
        if (cols.Count != 0)
        {
            AverageSurfaceSpeed = totalSpeed / cols.Count;
            AverageSurfaceFriction = totalFriction / cols.Count;
        }
        else
        {
            AverageSurfaceSpeed = 0.0f;
            AverageSurfaceFriction = 0.0f;
        }
    }

    #endregion
}
