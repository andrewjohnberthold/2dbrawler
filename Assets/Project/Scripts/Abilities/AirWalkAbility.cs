﻿using UnityEngine;
using System.Collections;

[DisallowMultipleComponent]
[RequireComponent(typeof(PlayerCollisionManager))]
[RequireComponent(typeof(Rigidbody2D))]
public class AirWalkAbility : Ability
{
    #region Public and Serialized Variables

    [Range(0, 20), Tooltip("")]
    public float Speed;

    [Range(0, 20), Tooltip("")]
    public float Strength;

    [Tooltip("")]
    public bool DisableVerticalAirWalking;

    #endregion

    #region Properties

    #endregion

    #region Private Variables

    private PlayerCollisionManager _pcm;
    private Rigidbody2D _rb;
    private IEnumerator _lastAirWalkRoutine;

    #endregion

    #region Initialization Methods

    private void Start()
    {
        _pcm = GetComponent<PlayerCollisionManager>();
        _rb = GetComponent<Rigidbody2D>();
    }

    #endregion

    #region Public Methods

    public void AirWalk(Vector2 input)
    {
        if (_lastAirWalkRoutine != null)
            StopCoroutine(_lastAirWalkRoutine);
        
        _lastAirWalkRoutine = AirWalkRoutine(input);
        StartCoroutine(_lastAirWalkRoutine);
    }

    #endregion

    #region Private Methods

    private Vector2 CalculateSpeed(Vector2 input)
    {
        Vector2 result = Speed * input;

        return result;
    }

    private Vector2 CalculateStrength(Vector2 input)
    {
        Vector2 result = Strength * input;

        if ((input.x < 0 && _pcm.IsColliding(Side.Left)) || (input.x > 0 && _pcm.IsColliding(Side.Right)))
            result.x = 0;

        return result;
    }

    private IEnumerator AirWalkRoutine(Vector2 input)
    {
        yield return new WaitForFixedUpdate();

        if (!CanAirWalk())
            yield break;

        if (DisableVerticalAirWalking)
            input.y = 0.0f;
        

        float currentVelocityX = _rb.velocity.x;
        float currentVelocityY = _rb.velocity.y;

        float desiredVelocityX = CalculateSpeed(input).x;
        float desiredVelocityY = CalculateSpeed(input).y;

        float calculatedStrengthX = CalculateStrength(input).x;
        float calculatedStrengthY = CalculateStrength(input).y;

        float neededVelocityX = desiredVelocityX - Mathf.Clamp(currentVelocityX, -Mathf.Abs(desiredVelocityX), Mathf.Abs(desiredVelocityX));
        float neededVelocityY = desiredVelocityY - Mathf.Clamp(currentVelocityY, -Mathf.Abs(desiredVelocityY), Mathf.Abs(desiredVelocityY));

        float cappedVelocityX = Mathf.Clamp(neededVelocityX, -Mathf.Abs(calculatedStrengthX), Mathf.Abs(calculatedStrengthX));
        float cappedVelocityY = Mathf.Clamp(neededVelocityY, -Mathf.Abs(calculatedStrengthY), Mathf.Abs(calculatedStrengthY));

        Vector2 force = new Vector2(cappedVelocityX,cappedVelocityY);

        _rb.AddForce(force, ForceMode2D.Impulse);

    }


    private bool CanAirWalk()
    {
        if (_pcm.IsColliding(Side.Bottom))
            return false;
        return true;
    }

    #endregion
}
