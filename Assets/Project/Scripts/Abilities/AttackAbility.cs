﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[RequireComponent(typeof(WalkAbility))]
[RequireComponent(typeof(GrabAbility))]
public class AttackAbility : Ability
{

    #region Public and Serialized Variables

    [Range(0, 10), Tooltip("")]
    public float AimDistance;

    [Range(0, 10), Tooltip("")]
    public float AimRadius;

    [Tooltip("")]
    public LayerMask AimLayerMask;

    public float AttackForce;


    #endregion

    #region Aiming Properties

    public Vector2 AimDirection { get; private set; }

    public Vector2 AimPointWorld
    {
        get
        {
            Vector2 origin = transform.position;
            Vector2 offset = AimDirection * AimDistance;
            return origin + offset;
        }
    }

    public Vector2 AimPointLocal
    {
        get
        {
            return AimDirection * AimDistance;
        }
    }

    #endregion

    #region
    #endregion

    #region
    #endregion

    #region
    #endregion

    #region Private Variables

    private PlayerStateManager _playerStateManager;
    private GrabAbility _grabAbility;
    #endregion

    #region Initialization Methods
    void Start()
    {
        _playerStateManager = GetComponent<PlayerStateManager>();
        _grabAbility = GetComponent<GrabAbility>();
    }
    #endregion


    #region Public Methods

    public void Aim(Vector2 input)
    {
        input.Normalize();
        if (input == Vector2.zero)
        {
            if (_playerStateManager.FacingDirection == Facing.Left)
                AimDirection = Vector2.left;
            else if (_playerStateManager.FacingDirection == Facing.Right)
                AimDirection = Vector2.right;
        }
        else
        {
            AimDirection = input;
        }
    }

    public void Attack()
    {
        if (!CanAttack())
            return;

        List<RaycastHit2D> targets = GetTargets().ToList();
        List<RaycastHit2D> validTargets = GetValidTargets(targets.ToArray()).ToList();

        foreach (RaycastHit2D target in targets)
        {
            if (validTargets.Contains(target))
            {
                target.rigidbody.DebugTrajectory(AimDirection * AttackForce, ForceMode2D.Impulse, Color.red, 2.0f, 2.0f);
                target.rigidbody.AddForce(AimDirection * AttackForce, ForceMode2D.Impulse);
            }
        }
    }


    #endregion
    
    #region Aiming Methods

    
    
    private RaycastHit2D[] GetValidTargets(RaycastHit2D[] targets)
    {
        List<RaycastHit2D> validTargets = new List<RaycastHit2D>();

        foreach (RaycastHit2D hit in targets)
        {
            // we hit ourselves ignore it.
            if (hit.transform == transform)
                continue;

            if (
                hit.transform.GetComponent<Knockable>() == null &&
                hit.transform.GetComponent<Damageable>() == null &&
                hit.transform.GetComponent<Stunnable>() == null &&
                hit.transform.GetComponent<Collectable>() == null
                )
            {
                continue;
            }
            
            // if we have passed all the tests and got this far, the hit is valid.
            validTargets.Add(hit);
        }

        return validTargets.ToArray();
    }

    private RaycastHit2D[] GetTargets()
    {
        Vector2 origin = (Vector2)transform.position + (AimDirection * AimDistance);
        float radius = AimRadius;
        Vector2 direction = AimDirection;
        float distance = AimDistance;
        LayerMask layerMask = AimLayerMask;

        List<RaycastHit2D> hits = Physics2D.CircleCastAll(origin, radius, direction, distance, layerMask).ToList();
        RaycastHit2D selfHit = new RaycastHit2D();
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.transform == transform)
                selfHit = hit;
        }
        hits.Remove(selfHit);
        return hits.ToArray();
    }

    private bool CanAttack()
    {
        //if stunned
        //  return false
        //if on cooldown
        //  retun false
        if (_grabAbility.IsHolding)
            return false;
        if (_grabAbility.LastThrowTime + 1.0f > Time.time)
            return false;

        return true;
    }

    #endregion

    #region Debug Methods
    public void OnDrawGizmos()
    {
        DrawDebugTargeting();
    }

    private void DrawDebugTargeting()
    {
        Vector2 player = transform.position;
        Vector2 aimPoint = AimPointWorld;
        Color successColor = Color.green;
        Color baseColor = Color.black;

        List<RaycastHit2D> targets = GetTargets().ToList();
        List<RaycastHit2D> validTargets = GetValidTargets(targets.ToArray()).ToList();

        foreach (RaycastHit2D target in targets)
        {
            Color color = baseColor;
            if (validTargets.Contains(target))
            {
                color = successColor;
                target.rigidbody.DrawTrajectory(AimDirection * AttackForce, ForceMode2D.Impulse, color, 2.0f);
            }
                

            Debug.DrawLine(AimPointWorld, target.point, color);
            DebugExtension.DrawCircle(target.point, Vector3.forward, color, 0.1f);
            Debug.DrawLine(target.point, target.transform.position, color);
            DebugExtension.DrawCircle(target.transform.position, Vector3.forward, color, 0.1f);
            
        }

        Color resultColor = Color.grey;
        if (targets.Count > 0)
            resultColor = baseColor;

        Debug.DrawLine(player, aimPoint, resultColor);
        DebugExtension.DrawCircle(player, Vector3.forward, resultColor, 0.1f);
        DebugExtension.DrawCircle(aimPoint, Vector3.forward, resultColor, 0.1f);
        DebugExtension.DrawCircle(aimPoint, Vector3.forward, resultColor, AimRadius);
    }
    #endregion
}
