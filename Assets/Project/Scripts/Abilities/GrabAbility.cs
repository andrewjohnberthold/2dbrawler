﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using ExtensionMethods;

[DisallowMultipleComponent]
[RequireComponent(typeof(PlayerStateManager))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(WalkAbility))]
public class GrabAbility : Ability
{
    #region Public and Serialized Variables

    [Header("Aiming Properties")]
    [Range(0, 10), Tooltip("")]
    public float AimDistance;

    [Range(0, 10), Tooltip("")]
    public float AimRadius;

    [Tooltip("")]
    public LayerMask AimLayerMask;

    [Header("Grabbing Properties")]
    [Range(0.01f, 1), Tooltip("")]
    public float GrabCooldown;

    [Header("Lifting Properties")]
    public float LiftDuration;

    public AnimationCurve LiftCurve;

    [Header("Holding Properties")]
    public Vector2 HoldAnchorOffset;

    [Header("Throwing Properties")]
    public float ThrowForce;
    public float KnockbackForce;

    public float IgnoreCollisionsDuration;

    #endregion

    #region Aiming Properties

    public Vector2 AimDirection { get; private set; }

    public Vector2 AimPointWorld
    {
        get
        {
            Vector2 origin = transform.position;
            Vector2 offset = AimDirection * AimDistance;
            return origin + offset;
        }
    }

    public Vector2 AimPointLocal
    {
        get
        {
            return AimDirection * AimDistance;
        }
    }

    #endregion

    #region Grabbing Properties

    public bool CanGrab
    {
        get
        {
            if (IsHolding)
                return false;
            if (GrabCooldownProgress < 1.0f)
                return false;
            return true;
        }
    }

    public float LastGrabTime { get; private set; }

    public float TimeSinceLastGrab
    {
        get { return Time.time - LastGrabTime; }
    }

    public float GrabCooldownProgress
    {
        get { return Mathf.Clamp01(TimeSinceLastGrab / GrabCooldown); }
    }

    #endregion

    #region Lifting Properties
    public bool IsLifting
    {
        get { return IsHolding && CurrentLiftProgress < 1.0f; }
    }
    public float CurrentLiftDuration
    {
        get { return IsHolding ? Mathf.Min(LiftDuration, Time.time - LastHoldStartTime) : 0.0f; }
    }

    public float CurrentLiftProgress
    {
        get { return IsHolding ? Mathf.Clamp01(CurrentHoldDuration / LiftDuration) : 0.0f; }
    }
    #endregion

    #region Holding Properties

    public bool IsHolding
    {
        get
        {
            return
              Joint &&
              HeldGameObject &&
              HeldGrabbable &&
              HeldTransform &&
              HeldRigidbody2D &&
              HeldCollider2D;
        }
    }

    public float LastHoldStartTime { get; private set; }

    public float CurrentHoldDuration
    {
        get { return IsHolding ? Time.time - LastHoldStartTime : 0.0f; }
    }
    
    public RelativeJoint2D Joint { get; private set; }

    public GameObject HeldGameObject { get; private set; }
    public Grabbable HeldGrabbable { get; private set; }
    public Transform HeldTransform { get; private set; }
    public Rigidbody2D HeldRigidbody2D { get; private set; }
    public Collider2D HeldCollider2D { get; private set; }

    #endregion

    #region Throwing Properties
    public bool CanThrow
    {
        get
        {
            if (!IsHolding)
                return false;
            return true;
        }
    }
    public float LastThrowTime { get; private set; }

    public float TimeSinceLastThrow
    {
        get { return Time.time - LastThrowTime; }
    }

    public float IgnoreCollisionsProgress
    {
        get { return Mathf.Clamp01(TimeSinceLastThrow / IgnoreCollisionsDuration); }
    }
    #endregion

    #region Private Variables

    private PlayerStateManager _playerStateManager;
    private Rigidbody2D _rigidbody2D;
    private Collider2D _collider2D;

    #endregion

    #region Initialization Methods
    void Start()
    {
        _playerStateManager = GetComponent<PlayerStateManager>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _collider2D = GetComponent<Collider2D>();
    }
    #endregion

    #region Public Methods

    public void Grab()
    {
        StopCoroutine(GrabRoutine());
        StartCoroutine(GrabRoutine());
    }

    public void Throw()
    {
        StopCoroutine(ThrowRoutine());
        StartCoroutine(ThrowRoutine());
    }

    #endregion

    #region Aiming Methods

    //TODO Make aiming pivit held objects in a little oval/circle above head. gives a little bit more feedback.
    //TODO Make the object we're about to hit glow in our colour.

    public void Aim(Vector2 input)
    {
        input.Normalize();
        if (input == Vector2.zero)
        {
            if (_playerStateManager.FacingDirection == Facing.Left)
                AimDirection = Vector2.left;
            else if (_playerStateManager.FacingDirection == Facing.Right)
                AimDirection = Vector2.right;
        }
        else
        {
            AimDirection = input;
        }
    }

    private RaycastHit2D GetClosestTarget(RaycastHit2D[] validTargets)
    {
        RaycastHit2D closest = new RaycastHit2D();
        float shortestDistance = Mathf.Infinity;
        foreach (RaycastHit2D hit in validTargets)
        {
            var hitDistance = Vector2.Distance(hit.point, AimPointWorld);
            if (hitDistance > shortestDistance)
                continue;

            shortestDistance = hitDistance;
            closest = hit;
        }

        return closest;
    }

    private RaycastHit2D[] GetValidTargets(RaycastHit2D[] targets)
    {
        List<RaycastHit2D> validTargets = new List<RaycastHit2D>();

        foreach (RaycastHit2D hit in targets)
        {
            // we hit ourselves ignore it.
            if (hit.transform == transform)
                continue;

            // if the object we hit does not have Grabbable behaviour, ignore it.
            var grabbable = hit.transform.gameObject.GetComponent<Grabbable>();
            if (grabbable == null)
                continue;
            
            // if the object is not grabbable, ignore it.
            if (grabbable.IsGrabbable == false)
                continue;

            // if we have passed all the tests and got this far, the hit is valid.
            validTargets.Add(hit);
        }

        return validTargets.ToArray();
    }

    private RaycastHit2D[] GetTargets()
    {
        Vector2 origin = transform.position;
        float radius = AimRadius;
        Vector2 direction = AimDirection;
        float distance = AimDistance;
        LayerMask layerMask = AimLayerMask;

        List<RaycastHit2D> hits = Physics2D.CircleCastAll(origin, radius, direction, distance, layerMask).ToList();
        RaycastHit2D selfHit = new RaycastHit2D();
        foreach (RaycastHit2D hit in hits)
        {
            if (hit.transform == transform)
                selfHit = hit;
        }
        hits.Remove(selfHit);
        return hits.ToArray();
    }

    #endregion

    #region Grabbing Methods
    
    private IEnumerator GrabRoutine()
    {
        yield return new WaitForFixedUpdate();

        if (!CanGrab)
            yield break;

        LastGrabTime = Time.time;

        RaycastHit2D[] targets = GetTargets();
        RaycastHit2D[] validTargets = GetValidTargets(targets);
        RaycastHit2D closestValidTarget = GetClosestTarget(validTargets);


        if (closestValidTarget.collider == null)
        {
            // we tried to grab but got nothing
            yield break;
        }

        LastHoldStartTime = Time.time;

        Joint = gameObject.AddComponent<RelativeJoint2D>();

        HeldGameObject = closestValidTarget.transform.gameObject;
        HeldGrabbable = closestValidTarget.transform.gameObject.GetComponent<Grabbable>();
        HeldTransform = closestValidTarget.transform;
        HeldRigidbody2D = closestValidTarget.rigidbody;
        HeldCollider2D = closestValidTarget.collider;

        
        Vector2 startingLinearOffset = transform.InverseTransformPoint(HeldGrabbable.transform.position);
        float startingAngularOffset = HeldTransform.eulerAngles.z;
        

        Joint.enableCollision = false;
        Joint.connectedBody = HeldRigidbody2D;
        Joint.maxForce = 10000;
        Joint.maxTorque = 10000;
        Joint.correctionScale = 0.3f;
        Joint.autoConfigureOffset = false;
        Joint.linearOffset = startingLinearOffset;

        StartCoroutine(LiftRoutine(startingLinearOffset, startingAngularOffset));
    }

    #endregion

    #region Lifting Methods

    private IEnumerator LiftRoutine(Vector2 startingLinearOffset, float startingAngularOffset)
    {
        while (IsLifting)
        {
            if (Joint.linearOffset != HoldAnchorOffset)
            {
                Vector2 newLinearOffset = Vector2.Lerp(startingLinearOffset, HoldAnchorOffset, LiftCurve.Evaluate(CurrentLiftProgress));
                Joint.linearOffset = newLinearOffset;
            }

            yield return new WaitForFixedUpdate();
        }

    }

    #endregion

        #region Holding Methods

        #endregion

        #region Throwing Methods

    private IEnumerator ThrowRoutine()
    {
        yield return new WaitForFixedUpdate();

        if (!CanThrow || !HeldGrabbable.IsThrowable)
            yield break;

        LastThrowTime = Time.time;

        Destroy(Joint);
        Joint = null;

        //GameObject thrownGameObject = HeldGameObject;
        Grabbable thrownGrabbable = HeldGrabbable;
        //Transform thrownTransform = HeldTransform;
        Collider2D thrownCollider2D = HeldCollider2D;
        Rigidbody2D thrownRigidbody2D = HeldRigidbody2D;

        HeldGameObject = null;
        HeldGrabbable = null;
        HeldTransform = null;
        HeldRigidbody2D = null;
        HeldCollider2D = null;

        Physics2D.IgnoreCollision(_collider2D, thrownCollider2D, true);

        yield return new WaitForFixedUpdate();

        Vector2 force = (-thrownRigidbody2D.velocity * thrownRigidbody2D.mass) + (AimDirection * ThrowForce);
        Vector2 knockbackForce = (AimDirection * -KnockbackForce) - _rigidbody2D.velocity;

        thrownGrabbable.IsThrown = true;

        StartCoroutine(DrawDebugThrow(thrownRigidbody2D, force, thrownGrabbable, 3.0f));

        thrownRigidbody2D.AddForce(force, ForceMode2D.Impulse);
        _rigidbody2D.AddForce(knockbackForce, ForceMode2D.Impulse);

        
        
        while (thrownGrabbable.IsThrown && IgnoreCollisionsProgress < 1.0f)
        {
            yield return new WaitForFixedUpdate();
        }
        
        Physics2D.IgnoreCollision(_collider2D, thrownCollider2D, false);
    }

    #endregion

    #region Debuging Methods

    public void OnDrawGizmos()
    {
        if (!IsHolding)
            DrawDebugTargeting();
        else
        {
            Vector2 force = (-HeldRigidbody2D.velocity * HeldRigidbody2D.mass) + (AimDirection * ThrowForce);
            HeldRigidbody2D.DrawTrajectory(force, ForceMode2D.Impulse, Color.green, 2.0f);
        }
    }

    private void DrawDebugTargeting()
    {
        Vector2 player = transform.position;
        Vector2 aimPoint = AimPointWorld;
        Color successColor = Color.green;
        Color failColor = Color.red;
        Color baseColor = Color.black;

        List<RaycastHit2D> targets = GetTargets().ToList();
        List<RaycastHit2D> validTargets = GetValidTargets(targets.ToArray()).ToList();
        RaycastHit2D closestValidTarget = GetClosestTarget(validTargets.ToArray());

        foreach (RaycastHit2D target in targets)
        {
            Color color = baseColor;
            if (closestValidTarget.collider == target.collider)
                color = successColor;
            else if (validTargets.Contains(target))
                color = failColor;

            Debug.DrawLine(AimPointWorld, target.point, color);
            DebugExtension.DrawCircle(target.point, Vector3.forward, color, 0.1f);
            Debug.DrawLine(target.point, target.transform.position, color);
            DebugExtension.DrawCircle(target.transform.position, Vector3.forward, color, 0.1f);
        }

        Color resultColor = Color.grey;
        if (closestValidTarget.collider != null)
            resultColor = successColor;
        else if (targets.Count > 0)
            resultColor = baseColor;

        Debug.DrawLine(player, aimPoint, resultColor);
        DebugExtension.DrawCircle(player, Vector3.forward, resultColor, 0.1f);
        DebugExtension.DrawCircle(aimPoint, Vector3.forward, resultColor, 0.1f);
        DebugExtension.DrawCircle(aimPoint, Vector3.forward, resultColor, AimRadius);
    }

    private IEnumerator DrawDebugThrow(Rigidbody2D thrownRigidbody2D, Vector2 force, Grabbable thrownGrabbable, float duration)
    {
        Vector2 startPosition = thrownRigidbody2D.transform.position;
        Vector2 startingVelocity = thrownRigidbody2D.velocity;

        thrownRigidbody2D.DebugTrajectory(force, ForceMode2D.Impulse, Color.red, 2.0f, duration);
        //DrawDebugTrajectory(thrownRigidbody2D, duration);

        Vector2 lastPosition = thrownRigidbody2D.transform.position;

        DebugExtension.DebugCircle(startPosition, Vector3.forward, Color.green, 0.1f, duration);

        while (thrownGrabbable.IsThrown)
        {
            Color color = Color.Lerp(Color.red, Color.black, thrownRigidbody2D.velocity.magnitude / startingVelocity.magnitude);
            Debug.DrawLine(lastPosition, thrownRigidbody2D.transform.position, color, duration);
            lastPosition = thrownRigidbody2D.transform.position;
            yield return new WaitForEndOfFrame();
        }


        DebugExtension.DebugCircle(lastPosition, Vector3.forward, Color.green, 0.1f, duration);
    }

    #endregion















}
