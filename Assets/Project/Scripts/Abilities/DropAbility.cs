﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

[DisallowMultipleComponent]
[RequireComponent(typeof(PlayerCollisionManager))]
[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(GrabAbility))]
public class DropAbility : Ability
{
    #region Public and Serialized Variables

    [Range(0.0f, 1.0f), Tooltip("Number of seconds the player must wait after successfully dropping before they can attempt to drop again.")]
    public float Cooldown = 0.5f;

    [Range(0.0f, 1.0f), Tooltip("Number of seconds, after dropping, that collisions will be ignored between the player (plus any held objects) and any platforms at their feet.")]
    public float DropDuration = 0.5f;

    [Range(0.0f, 10.0f), Tooltip("Additional boost to push the player down through the platform after dropping.")]
    public float DropForce = 10.0f;

    [Range(-1.0f, 1.0f), Tooltip("Maximum Y Axis input allowed for a drop to register. Values less than 0.00 require the player to hold down to drop. A Value of 1.00 ignores Y Axis input.")]
    public float MaxInputY;


    #endregion

    #region Properties

    public float LastDropTime { get; private set; }

    public float TimeSinceLastDrop
    {
        get { return Time.time - LastDropTime; }
    }

    public bool CooldownComplete
    {
        get { return TimeSinceLastDrop > Cooldown; }
    }

    public bool IsDropping
    {
        get { return TimeSinceLastDrop < DropDuration; }
    }

    public bool IsOnPlatform
    {
        get
        {
            bool result = false;
            foreach (var col in _pcm.GetCollisions(Side.Bottom, true, false))
            {
                if (col.HitPlatformSurface && col.Hit.transform.GetComponent<PlatformEffector2D>().useOneWay)
                    result = true;
            }
            return result;
        }
    }


    #endregion

    #region Private Variables

    private PlayerCollisionManager _pcm;
    private Rigidbody2D _rb;
    private GrabAbility _grabAbility;
    private IEnumerator _lastDropRoutine;

    #endregion

    #region Initialization Methods

    private void Start()
    {
        _pcm = GetComponent<PlayerCollisionManager>();
        _rb = GetComponent<Rigidbody2D>();
        _grabAbility = GetComponent<GrabAbility>();
    }

    #endregion

    #region Public Methods

    public void Drop(float input)
    {
        if (_lastDropRoutine != null)
            StopCoroutine(_lastDropRoutine);

        _lastDropRoutine = DropRoutine(input);
        StartCoroutine(_lastDropRoutine);
    }

    #endregion

    #region Private Methods

    private IEnumerator DropRoutine(float input)
    {
        yield return new WaitForFixedUpdate();

        if (!CanDrop() || input > MaxInputY)
            yield break;

        LastDropTime = Time.time;

        int platforms = 0;
        List<PlayerCollision> cols = _pcm.GetCollisions(Side.Bottom, true, false);

        foreach (var col in cols)
        {
            if (col.HitPlatformSurface && col.Hit.transform.GetComponent<PlatformEffector2D>().useOneWay)
            {
                StartCoroutine(IgnoreCollisionsRoutine(col.Hit.collider));
                platforms ++;
            }
        }

        if (platforms > 0)
        {
            _rb.velocity = new Vector2(_rb.velocity.x, 0.0f);
            _rb.AddForce(Vector2.down * DropForce, ForceMode2D.Impulse);
        }
    }

    private IEnumerator IgnoreCollisionsRoutine(Collider2D col)
    {
        Physics2D.IgnoreCollision(_pcm.Collider, col, true);

        List<Collider2D> heldColliders = new List<Collider2D>();
        if (_grabAbility.IsHolding)
        {
            heldColliders.Add(_grabAbility.HeldCollider2D);
        }

        foreach (var heldCollider in heldColliders)
        {
            Physics2D.IgnoreCollision(heldCollider, col, true);
        }

        yield return new WaitForSeconds(DropDuration);

        Physics2D.IgnoreCollision(_pcm.Collider, col, false);
        foreach (var heldCollider in heldColliders)
        {
            Physics2D.IgnoreCollision(heldCollider, col, false);
        }
    }


    private bool CanDrop()
    {
        //if(Stunned)
        //  return false;
        if (!CooldownComplete)
            return false;
        if (!IsOnPlatform)
            return false;
        return true;
    }
    #endregion

    #region Debugging Methods

    #endregion

    
}
