﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;
using UnityEngine.UI;

[ExecuteInEditMode]
public class DebugingPanel : MonoBehaviour
{

    public Text General;
    public Text Movement;
    public Text Jumping;
    public Text Grabbing;

    private PlayerInputManager _pim;
    private PlayerStateManager _psm;
    private PlayerCollisionManager _pcm;
    private JumpAbility _ja;
    private WalkAbility _ma;
    private GrabAbility _ga;
    private Rigidbody2D _rb;

    void Awake()
    {
        _pim = GetComponent<PlayerInputManager>();
        _psm = GetComponent<PlayerStateManager>();
        _pcm = GetComponent<PlayerCollisionManager>();
        _ja = GetComponent<JumpAbility>();
        _ma = GetComponent<WalkAbility>();
        _ga = GetComponent<GrabAbility>();
        _rb = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        General.text = "";
        General.text += "Player Number: " + _pim.PlayerNumber+ "\n";
        General.text += "Grounded: " + _pcm.IsColliding(Side.Bottom) + "\n";
        General.text += "Roofed: " + _pcm.IsColliding(Side.Top) + "\n";
        General.text += "WalledLeft: " + _pcm.IsColliding(Side.Left) + "\n";
        General.text += "WalledRight: " + _pcm.IsColliding(Side.Right) + "\n";

        Movement.text = "";
        Movement.text += "Speed: " + _ma.BaseSpeed + "\n";
        Movement.text += "X Velocity: " + _rb.velocity.x.ToString("F2") + "\n";
        Movement.text += "Strength: " + _ma.MaxStrength + "\n";
        Movement.text += "Min Strength: " + _ma.MinStrength + "\n";
        Movement.text += "AverageSurfaceSpeed: " + _ma.AverageSurfaceSpeed.ToString("F2") + "\n";
        Movement.text += "AverageSurfaceFriction: " + _ma.AverageSurfaceFriction.ToString("F2") + "\n";
        Movement.text += "Speed: " + _ma.BaseSpeed + "\n";
        Movement.text += "Facing: " + _psm.FacingDirection + "\n";

        Jumping.text = "";
        Jumping.text += "Jumping: " + _ja.IsJumping + "\n";
        Jumping.text += "Count: " + _ja.JumpCount+ " / " + _ja.MaxJumps + "\n";
        Jumping.text += "Time: " + _ja.MinJumpDuration.ToString("F2") + "s / " + _ja.CurrentJumpDuration.ToString("F2") + "s / " +
                        _ja.MaxJumpDuration.ToString("F2") + "s = " + _ja.CurrentJumpDurationProgress.ToString("P0") + "\n";

        Grabbing.text = "";
        Grabbing.text += "Lifting: " + _ga.IsLifting + ", Holding: "+ _ga.IsHolding + "\n";
        Grabbing.text += "Last Grab Time: " + _ga.LastGrabTime.ToString("F2") + "\n";
        Grabbing.text += "Grab Cooldown: " + _ga.TimeSinceLastGrab.ToString("F2") + "s / "+ _ga.GrabCooldown.ToString("F2") + "s = " + _ga.GrabCooldownProgress.ToString("P0") + "\n";
        Grabbing.text += "Can Grab: " + _ga.CanGrab+ ", Can Throw: " + _ga.CanThrow + "\n";
        Grabbing.text += "Last Hold Start Time: " + _ga.LastHoldStartTime.ToString("F2") + "\n";
        Grabbing.text += "Lift DropDuration: " + _ga.CurrentLiftDuration.ToString("F2") + "s / " + _ga.LiftDuration.ToString("F2") + "s = " + _ga.CurrentLiftProgress.ToString("P0") + "\n";
        Grabbing.text += "Hold DropDuration: " + _ga.CurrentHoldDuration.ToString("F2") + "s \n";
    }
}
